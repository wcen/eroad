package nz.co.test

data class AccelerometerData(
    val x_acc: Float,
    val y_acc: Float,
    val z_acc: Float,
    val t_sec: Long = System.currentTimeMillis()
)