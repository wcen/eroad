package nz.co.test

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import com.google.gson.Gson
import org.joda.time.DateTime
import org.joda.time.Interval
import org.joda.time.format.DateTimeFormat
import java.io.*


open class RecordManager(private val context: Context): SensorEventListener {

    companion object {
        private const val FILE_NAME_PREFIX = "Sensor_"
        private const val FILE_NAME_SUFFIX = ".sns"
        private val FILE_NAME_DATE_TIME_FORMATTER = DateTimeFormat.forPattern("YYYY-MM-dd-hh-mm-ss-SSS")
        private const val INTERVAL_BETWEEN_FILES_MINUTES = 3
    }

    private var sensorManager: SensorManager? = null
    private lateinit var currentFileName: String
    private lateinit var currentData: RecordFileData
    private val handlerThread = HandlerThread("WriteFileThread")
    private lateinit var handler: Handler

    fun start() {
        handlerThread.start()
        handler = Handler(handlerThread.looper)

        val latestFileName = retrieveLatestFileName()
        val nowTime = DateTime.now()

        if (latestFileName != null) {
            val latestFileCreateTime = fileCreateTimeFrom(latestFileName)

            val interval = Interval(latestFileCreateTime, nowTime)

            val latestRecordFileData = retrieveRecordedDataFrom(latestFileName)

            if (interval.toDuration().standardMinutes > INTERVAL_BETWEEN_FILES_MINUTES || latestRecordFileData == null) {
                currentFileName = generateFileNameFrom(nowTime)
                currentData = RecordFileData(start = nowTime.millis)
            } else {
                currentData = latestRecordFileData
                currentFileName = latestFileName
            }
        } else {
            currentFileName = generateFileNameFrom(nowTime)
            currentData = RecordFileData(start = nowTime.millis)
        }

        sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val accelerometer = sensorManager?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        sensorManager?.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_FASTEST)
    }

    fun stop() {
        sensorManager?.unregisterListener(this)
        Log.v("RecordManager", "### stop!")
        handlerThread.quitSafely()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    abstract class WriteFileRunnable(val fileName: String, val data: RecordFileData, val context: Context): Runnable

    override fun onSensorChanged(event: SensorEvent?) {
        event?.values?.let { values ->
            try {
                if (DateTime(currentData.end).isBeforeNow) {
                    val nowTime = DateTime.now()
                    currentFileName = generateFileNameFrom(nowTime)
                    currentData = RecordFileData(start = nowTime.millis)
                }
                currentData.sensorData.add(AccelerometerData(values[0], values[1], values[2]))

                // get copies of all these parameters because it may be overwritten before the runnable gets executed
                handler.post(object : WriteFileRunnable(currentFileName, currentData.copy(), context) {
                    override fun run() {
                        overwriteDataInFileWith(currentFileName, currentData.copy(), context)
                    }
                })

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /* I understand that the requirement says "Implement a method which takes start and end times as parameter"
    * instead, i wrapped all these information inside data: RecordFileData because it is easier for me to
    * add new reading of accelerometer and write all the data in JSON format with gson lib at once. Gson
    * is also having performance issue which can be improved by manually constructing the JSON string.
    */
    @Throws(Exception::class)
    fun overwriteDataInFileWith(fileName: String, data: RecordFileData, context: Context) {
        val currentFile = File(context.filesDir, fileName)
        currentFile.createNewFile()

        val fileWriter = FileWriter(
            currentFile,
            false
        )
        fileWriter.write(Gson().toJson(data, RecordFileData::class.java))
        fileWriter.flush()
        fileWriter.close()
    }

    @Throws(IllegalArgumentException::class)
    private fun fileCreateTimeFrom(fileName: String): DateTime {
        val fileNameDateString = fileName.removePrefix(FILE_NAME_PREFIX).removeSuffix(FILE_NAME_SUFFIX)
        return FILE_NAME_DATE_TIME_FORMATTER.parseDateTime(fileNameDateString)
    }

    private fun generateFileNameFrom(time: DateTime): String {
        return FILE_NAME_PREFIX + FILE_NAME_DATE_TIME_FORMATTER.print(time) + FILE_NAME_SUFFIX
    }

    private fun retrieveRecordedDataFrom(fileName: String): RecordFileData? {
        try {
            val inputStream: InputStream? = context.openFileInput(fileName)
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream)
                val bufferedReader = BufferedReader(inputStreamReader)
                var receiveString = bufferedReader.readLine()
                val stringBuilder = StringBuilder()

                while (receiveString != null) {
                    stringBuilder.append("\n").append(receiveString)
                    receiveString = bufferedReader.readLine()
                }
                inputStream.close()
                val dataString = stringBuilder.toString()

                return Gson().fromJson(dataString, RecordFileData::class.java)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

    private fun retrieveLatestFileName(): String? {
        val fileList = context.fileList()
        fileList.sortWith(Comparator<String> { fileName1, fileName2 ->
            try {
                val fileNameDate1 = fileCreateTimeFrom(fileName1)
                val fileNameDate2 = fileCreateTimeFrom(fileName2)
                when {
                    fileNameDate1.isAfter(fileNameDate2) -> {
                        1
                    }
                    fileNameDate1.isBefore(fileNameDate2) -> {
                        -1
                    }
                    else -> {
                        0
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                0
            }
        })
        return fileList.lastOrNull()
    }
}