package nz.co.test

import android.app.Service
import android.content.Intent
import android.os.IBinder

class RecordService : Service() {
    private var recordManager: RecordManager? = null
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        recordManager = RecordManager(this)
        recordManager?.start()
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        recordManager?.stop()
    }
}