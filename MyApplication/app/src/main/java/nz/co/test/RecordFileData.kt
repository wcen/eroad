package nz.co.test

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RecordFileData(
    val start: Long = System.currentTimeMillis(),
    val end: Long = start + 180000,
    val sensorData: MutableList<AccelerometerData> = mutableListOf()
)